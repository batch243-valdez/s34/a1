
const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// GET /home

app.get("/home", (req, res) => {
    
    res.send(`Welcome to the Home page!`);
});



// GET /users

const users = [
    {
      firstName: "Bazil",
      age: 18,
    },
    {
      firstName: "Bangs",
      age: 25,
    },
  ];

app.get("/users", (req, res) => {
    
    res.json(users);
});


// Delete delete-user


app.delete("/delete-user", (req, res) => {
    if (users.includes(req.body.firstName)){
        users.pop();
        res.send("User has been deleted!");
    }
    else {
        res.send("User is not yet registered!");
    }
    });
  
  
app.listen(port, () => {
console.log(`the server is Listening to Port ${port}`);
});